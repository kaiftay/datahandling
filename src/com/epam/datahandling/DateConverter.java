package com.epam.datahandling;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;

public class DateConverter {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("eeee, MMM dd, yyyy hh:mm:ss a", Locale.ENGLISH);

    public static String convertDate(String date){
      return ISO_LOCAL_DATE.format(FORMATTER.parse(date));
    }

    public static void main(String[] args) {
        System.out.println(convertDate("Wednesday, Dec 12, 2018 10:10:10 AM"));
    }
}
