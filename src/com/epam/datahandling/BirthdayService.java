package com.epam.datahandling;

import java.time.LocalDate;
import java.time.Period;

public class BirthdayService {
    public static int year(LocalDate localDate){
        return Period.between(localDate, LocalDate.now()).getYears();
    }

    public static void main(String[] args) {
        System.out.println(BirthdayService.year(LocalDate.of(1996,5, 6)));
    }

}
