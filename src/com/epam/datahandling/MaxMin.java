package com.epam.datahandling;

import java.util.Arrays;

public class MaxMin {
    public static void printMinMax(double... numbers) {
        Arrays.sort(numbers);
        System.out.println("min: " + numbers[0]);
        System.out.println("max: " + numbers[numbers.length-1]);
    }

    public static void main(String[] args){
        printMinMax(43, 24, 52,100,43);
    }
}