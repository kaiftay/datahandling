package com.epam.datahandling;

import java.math.BigDecimal;

public class Numbers {
    public static boolean isSum(String str) {
        String[] numbers = str.split(" ");
        BigDecimal[] array = new BigDecimal[3];
        for (int i = 0; i < 3; i++) {
            array[i] = new BigDecimal(numbers[i]);
        }
        return array[2].equals(array[1].add(array[0]));
    }

    public static void main(String[] args) {
        System.out.println(isSum("0.1 0.2 0.3"));
    }

}
