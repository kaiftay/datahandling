package com.epam.datahandling;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateDifference {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    public static long dayBetween(String firstDate, String secondDate){
        LocalDate first = LocalDate.from(FORMATTER.parse(firstDate));
        LocalDate second = LocalDate.from(FORMATTER.parse(secondDate));
        return Math.abs(ChronoUnit.DAYS.between(first, second));
    }

    public static void main(String[] args) {
        System.out.println(dayBetween("01.10.1010","01.10.2010"));
    }
}
