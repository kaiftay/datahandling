package com.epam.datahandling;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Square {
    private static final BigDecimal PI = BigDecimal.valueOf(Math.PI);
    private static final MathContext CONTEXT = new MathContext(50, RoundingMode.HALF_EVEN);

    public static BigDecimal radius(double radius) {
        BigDecimal r = new BigDecimal(radius, CONTEXT);
        return r.multiply(r, CONTEXT).multiply(PI, CONTEXT);
    }
    public static void main(String[] args) {
        System.out.println(radius(3.123456789));
    }
}